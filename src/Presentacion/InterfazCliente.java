
package Presentacion;

import Datos.Cliente;
import Negocios.NegociosCliente;
import java.awt.Color;
import java.security.NoSuchAlgorithmException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InterfazCliente extends javax.swing.JFrame {

    public InterfazCliente() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.getContentPane().setBackground(Color.DARK_GRAY);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtNombreC = new javax.swing.JTextField();
        txtTelefonoC = new javax.swing.JTextField();
        txtIdCliente = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbClientes = new javax.swing.JTable();
        btnRegistrar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 204, 153));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 55)); // NOI18N
        jLabel1.setText("Clientes");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 30, -1, -1));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel2.setText("Nombre:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 210, -1, -1));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel4.setText("Telefono:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 320, -1, -1));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel10.setText("ID usuario");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 690, -1, -1));

        txtNombreC.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtNombreC.setName(""); // NOI18N
        txtNombreC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreCKeyTyped(evt);
            }
        });
        jPanel1.add(txtNombreC, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 190, 410, 40));

        txtTelefonoC.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtTelefonoC.setName(""); // NOI18N
        txtTelefonoC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoCKeyTyped(evt);
            }
        });
        jPanel1.add(txtTelefonoC, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 310, 410, 40));

        txtIdCliente.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtIdCliente.setName(""); // NOI18N
        jPanel1.add(txtIdCliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 680, 410, 40));

        jPanel2.setBackground(new java.awt.Color(255, 204, 204));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tbClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbClientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbClientes);

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 570, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 180, 610, 470));

        btnRegistrar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 670, 130, 40));

        btnMostrar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnMostrar.setText("Mostrar Tabla");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnMostrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1340, 130, 190, 40));

        btnActualizar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });
        jPanel1.add(btnActualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 670, 130, 40));

        btnEliminar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jPanel1.add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 670, 130, 40));

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_icon-icons.com_74448.png"))); // NOI18N
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1660, 680, 40, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, 1810, 810));

        btnSalir.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1730, 910, 130, 40));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreCKeyTyped
        char c=evt.getKeyChar();
        if((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>=' ' && c<=' ') || (c=='ñ' || c== 'Ñ')){

        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtNombreCKeyTyped

    private void txtTelefonoCKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoCKeyTyped
        char c=evt.getKeyChar();
        if(c<'0' || c>'9'){
            evt.consume();
        }
    }//GEN-LAST:event_txtTelefonoCKeyTyped

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        //String contra=txtContraseñaU.getPassword().toString();
        if(txtNombreC.getText().length()==0 || txtTelefonoC.getText().length()==0){

            JOptionPane.showMessageDialog(null, "Debes llenar todos los campos ");
        }else{

            try {
                Cliente persona=new Cliente();
                persona.setNombre(txtNombreC.getText().toUpperCase());
                persona.setTelefono(txtTelefonoC.getText().toUpperCase());
         

                new NegociosCliente().PuenteRegistrar(persona);
                Limpiar();
            }catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
        }
        btnMostrar.doClick();
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        //btnImagen.setVisible(false);
        NegociosCliente tbl=new NegociosCliente();
        try {
            tbl.PuenteMostrarTabla(tbClientes);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        tbClientes.setVisible(true);
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        try {
            Cliente usu=new Cliente();
            usu.setNombre(txtNombreC.getText().toUpperCase());
            usu.setTelefono(txtTelefonoC.getText().toUpperCase());    
            usu.setIdCliente(Integer.parseInt(txtIdCliente.getText()));

            new NegociosCliente().Actualizar(usu);
            Limpiar();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        btnMostrar.doClick();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if(txtIdCliente.getText().length()!=0 ){
            try {
                Cliente usu=new Cliente();
                usu.setIdCliente(Integer.parseInt(txtIdCliente.getText()));

                new NegociosCliente().PuenteEliminar(usu);
            } catch (ClassNotFoundException ex) {
                //Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(rootPane,"El Cliente que se intenta eliminar no existe");
            } catch (InstantiationException ex) {
                //  Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(rootPane,"Error al intentar eliminar");
            } catch (IllegalAccessException ex) {
                // Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(rootPane,"Error al intentar eliminar");
            }
        }
        txtIdCliente.setText("");
        btnMostrar.doClick();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        //btnImagen.setVisible(false);
        if(txtIdCliente.getText().length()!=0){
            try {
                Cliente con=new Cliente();
                con.setIdCliente(Integer.parseInt(txtIdCliente.getText().toUpperCase()));
                NegociosCliente nc=new NegociosCliente();
                nc.PuenteBuscar(con);
                //Si funciona la llamada al metodo
                txtNombreC.setText(con.getNombre());
                txtTelefonoC.setText(con.getTelefono());

            } catch (InstantiationException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
            catch (ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
            catch (IllegalAccessException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }

        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void tbClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbClientesMouseClicked
           if(evt.getButton()==1){
            int fila=tbClientes.getSelectedRow();
            txtIdCliente.setText(tbClientes.getValueAt(fila,0).toString());
            txtNombreC.setText(tbClientes.getValueAt(fila,1).toString());
            txtTelefonoC.setText(tbClientes.getValueAt(fila,2).toString()); 
        }
    }//GEN-LAST:event_tbClientesMouseClicked

     public void Limpiar(){
        txtNombreC.setText("");
        txtTelefonoC.setText("");      
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfazCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbClientes;
    private javax.swing.JTextField txtIdCliente;
    private javax.swing.JTextField txtNombreC;
    private javax.swing.JTextField txtTelefonoC;
    // End of variables declaration//GEN-END:variables
}
