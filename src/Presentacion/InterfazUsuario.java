package Presentacion;
import Datos.Usuario;
import Negocios.NegociosUsuario;
import java.awt.Color;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class InterfazUsuario extends javax.swing.JFrame {
    public InterfazUsuario() {
        initComponents();
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.getContentPane().setBackground(Color.DARK_GRAY);
        lbTipo.setVisible(false);
        lbTipo.setEnabled(false);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSalir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnBuscar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNombreUsuario = new javax.swing.JTextField();
        txtNombreU = new javax.swing.JTextField();
        txtDireccionU = new javax.swing.JTextField();
        txtTelefonoU = new javax.swing.JTextField();
        cbTipoUsuario = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        btnImagen = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbUsuarios = new javax.swing.JTable();
        btnRegistrar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtIdUsuario = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtContraseñaU = new javax.swing.JPasswordField();
        lbTipo = new javax.swing.JLabel();
        btnActualizar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnSalir.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1730, 950, 130, 40));

        jPanel1.setBackground(new java.awt.Color(87, 240, 226));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/search_icon-icons.com_74448.png"))); // NOI18N
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1660, 680, 40, -1));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel2.setText("Nombre:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 200, -1, -1));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel3.setText("Direccion:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 260, -1, -1));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel4.setText("Telefono:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 320, -1, -1));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel5.setText("Tipo de Usuario:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 380, -1, -1));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel6.setText("Contraseña:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 440, -1, -1));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel7.setText("Nombre de Usuario:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 500, -1, -1));

        txtNombreUsuario.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtNombreUsuario.setName(""); // NOI18N
        jPanel1.add(txtNombreUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 490, 410, 40));

        txtNombreU.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtNombreU.setName(""); // NOI18N
        txtNombreU.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreUKeyTyped(evt);
            }
        });
        jPanel1.add(txtNombreU, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 190, 410, 40));

        txtDireccionU.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtDireccionU.setName(""); // NOI18N
        jPanel1.add(txtDireccionU, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 250, 410, 40));

        txtTelefonoU.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtTelefonoU.setName(""); // NOI18N
        txtTelefonoU.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoUKeyTyped(evt);
            }
        });
        jPanel1.add(txtTelefonoU, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 310, 410, 40));

        cbTipoUsuario.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        cbTipoUsuario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Cajero", "Administrador" }));
        cbTipoUsuario.setName(""); // NOI18N
        jPanel1.add(cbTipoUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 380, 410, 30));

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnImagen.setBackground(new java.awt.Color(255, 255, 255));
        btnImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/tumblr_ohvzxtnG7B1rp8h87o1_r2_400.gif"))); // NOI18N
        jPanel2.add(btnImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 690, 410));

        tbUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbUsuariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbUsuarios);

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 690, 400));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 160, 710, 430));

        btnRegistrar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnRegistrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 670, 130, 40));

        btnMostrar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnMostrar.setText("Mostrar Tabla");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        jPanel1.add(btnMostrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1320, 100, 190, 40));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel8.setText("Buscar un Usuario");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1360, 630, -1, -1));

        txtIdUsuario.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        txtIdUsuario.setName(""); // NOI18N
        jPanel1.add(txtIdUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(1240, 680, 410, 40));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel10.setText("ID usuario");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(1120, 690, -1, -1));
        jPanel1.add(txtContraseñaU, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 430, 410, 40));

        lbTipo.setEnabled(false);
        jPanel1.add(lbTipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 570, 50, 60));

        btnActualizar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });
        jPanel1.add(btnActualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 670, 130, 40));

        btnEliminar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        jPanel1.add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 670, 130, 40));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 3, 60)); // NOI18N
        jLabel9.setText("Usuarios");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 10, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 57, 1845, 830));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        btnImagen.setVisible(false);
        NegociosUsuario tbl=new NegociosUsuario();
        try {
            tbl.PuenteMostrarTabla(tbUsuarios);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        tbUsuarios.setVisible(true);
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
            try {
            Usuario usu=new Usuario();
            usu.setNombre(txtNombreU.getText().toUpperCase());
            usu.setDireccion(txtDireccionU.getText().toUpperCase());
            usu.setTelefono(txtTelefonoU.getText().toUpperCase());
            usu.setContraseña(txtContraseñaU.getPassword().toString().toUpperCase());
            usu.setTipousuario(cbTipoUsuario.getSelectedItem().toString().toUpperCase());
            usu.setNombreu(txtNombreUsuario.getText().toUpperCase());
            usu.setIdUsuario(Integer.parseInt(txtIdUsuario.getText()));

            new NegociosUsuario().Actualizar(usu);
            Limpiar();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        btnMostrar.doClick();
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        //String contra=txtContraseñaU.getPassword().toString();
        if(txtNombreU.getText().length()==0 || txtDireccionU.getText().length() ==0|| txtTelefonoU.getText().length()==0||  txtContraseñaU.getText().length()==0|| txtNombreUsuario.getText().length()==0){

            JOptionPane.showMessageDialog(null, "Debes llenar todos los campos ");
        }else{

            try {
                Usuario persona=new Usuario();
                persona.setNombre(txtNombreU.getText().toUpperCase());
                persona.setDireccion(txtDireccionU.getText().toUpperCase());
                persona.setTelefono(txtTelefonoU.getText().toUpperCase());
                persona.setTipousuario(cbTipoUsuario.getSelectedItem().toString().toUpperCase());
                // persona.setContraseña(String.valueOf(contra.toUpperCase().hashCode()));//cambiar
                persona.setContraseña(txtContraseñaU.getPassword().toString().toUpperCase());
                persona.setNombreu(txtNombreUsuario.getText().toUpperCase());

                new NegociosUsuario().PuenteRegistrar(persona);
                Limpiar();
            }catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
        }
        btnMostrar.doClick();
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void tbUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbUsuariosMouseClicked
        if(evt.getButton()==1){
            int fila=tbUsuarios.getSelectedRow();
            lbTipo.setText(tbUsuarios.getValueAt(fila,4).toString());
            if("ADMINISTRADOR".equals(lbTipo.getText())){
                cbTipoUsuario.setSelectedIndex(1);
            }
            if("CAJERO".equals(lbTipo.getText())){
                cbTipoUsuario.setSelectedIndex(0);
            }
            txtIdUsuario.setText(tbUsuarios.getValueAt(fila,0).toString());
            txtNombreU.setText(tbUsuarios.getValueAt(fila,1).toString());
            txtDireccionU.setText(tbUsuarios.getValueAt(fila,2).toString());
            txtTelefonoU.setText(tbUsuarios.getValueAt(fila,3).toString());
            txtContraseñaU.setText(tbUsuarios.getValueAt(fila,5).toString());
            txtNombreUsuario.setText(tbUsuarios.getValueAt(fila,6).toString());
        }
    }//GEN-LAST:event_tbUsuariosMouseClicked

    private void txtTelefonoUKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoUKeyTyped
        char c=evt.getKeyChar();
        if(c<'0' || c>'9'){
            evt.consume();
        }
    }//GEN-LAST:event_txtTelefonoUKeyTyped

    private void txtNombreUKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreUKeyTyped
        char c=evt.getKeyChar();
        if((c>='a' && c<='z') || (c>='A' && c<='Z') || (c>=' ' && c<=' ') || (c=='ñ' || c== 'Ñ')){

        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtNombreUKeyTyped

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        btnImagen.setVisible(false);
        if(txtIdUsuario.getText().length()!=0){
            try {
                Usuario con=new Usuario();
                con.setIdUsuario(Integer.parseInt(txtIdUsuario.getText().toUpperCase()));
                NegociosUsuario nc=new NegociosUsuario();
                nc.PuenteBuscar(con);
                //Si funciona la llamada al metodo
                txtNombreU.setText(con.getNombre());
                txtDireccionU.setText(con.getDireccion());
                txtTelefonoU.setText(con.getTelefono());
                txtNombreUsuario.setText(con.getNombreu());
            } catch (InstantiationException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
            catch (ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
            catch (IllegalAccessException e) {
                JOptionPane.showMessageDialog(null, "Error: "+e);
            }
          
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
       if(txtIdUsuario.getText().length()!=0 ){
            try {
                Usuario usu=new Usuario();
                usu.setIdUsuario(Integer.parseInt(txtIdUsuario.getText()));

                new NegociosUsuario().PuenteEliminar(usu);
            } catch (ClassNotFoundException ex) {
                //Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(rootPane,"El usuario que se intenta eliminar no existe  no existe");
            } catch (InstantiationException ex) {
              //  Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(rootPane,"Error al intentar eliminar");
            } catch (IllegalAccessException ex) {
               // Logger.getLogger(InterfazUsuario.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(rootPane,"Error al intentar eliminar");
            } 
        }
       txtIdUsuario.setText("");
       btnMostrar.doClick();
    }//GEN-LAST:event_btnEliminarActionPerformed
    public void Limpiar(){
        txtNombreU.setText("");
        txtDireccionU.setText("");
        txtTelefonoU.setText("");      
        txtContraseñaU.setText("");  
        cbTipoUsuario.setSelectedIndex(0);
        txtNombreUsuario.setText("");
    }
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfazUsuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnImagen;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cbTipoUsuario;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbTipo;
    private javax.swing.JTable tbUsuarios;
    private javax.swing.JPasswordField txtContraseñaU;
    private javax.swing.JTextField txtDireccionU;
    private javax.swing.JTextField txtIdUsuario;
    private javax.swing.JTextField txtNombreU;
    private javax.swing.JTextField txtNombreUsuario;
    private javax.swing.JTextField txtTelefonoU;
    // End of variables declaration//GEN-END:variables
}
