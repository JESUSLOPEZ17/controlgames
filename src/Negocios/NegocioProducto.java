package Negocios;

import Datos.MantenimientoProducto;
import Datos.Producto;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class NegocioProducto {
     MantenimientoProducto con;
    
    public void PuenteRegistrar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        try {
            con=new MantenimientoProducto();
            con.Registrar(pro);
            
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void PuenteBuscar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoProducto mc=new MantenimientoProducto();
            mc.Buscar(pro);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void Actualizar(Producto pro){
        try {
            MantenimientoProducto ac=new MantenimientoProducto();
            ac.Actualizar(pro);           
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
        }
    }
    public void PuenteEliminar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
          try {
            int n=JOptionPane.showConfirmDialog(null,"Confirmar --¿Eliminar Registro?");
            if(n==0){   
            MantenimientoProducto mc=new MantenimientoProducto();
            mc.Eliminar(pro);
            }
        }catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public JTable PuenteMostrarTabla (JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        con=new MantenimientoProducto();
        JTable tbl;
        tbl=con.MostrarTabla(tabla);
        
        return tbl; //retornar la tabla llena a la interfaz automovil
    }
    public JTable PuenteBuscarProducto(JTable tb,String cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoProducto mc=new MantenimientoProducto();
            mc.BuscarTb(tb,cte);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        return tb;
    }
}
