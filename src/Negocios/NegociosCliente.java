package Negocios;

import Datos.Cliente;
import Datos.MantenimientoCliente;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class NegociosCliente {
     MantenimientoCliente con;
    
    public void PuenteRegistrar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        try {
            con=new MantenimientoCliente();
            con.Registrar(cte);
            
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void PuenteBuscar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoCliente mc=new MantenimientoCliente();
            mc.Buscar(cte);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void Actualizar(Cliente cte){
        try {
            MantenimientoCliente ac=new MantenimientoCliente();
            ac.Actualizar(cte);           
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
        }
    }
    public void PuenteEliminar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
          try {
            int n=JOptionPane.showConfirmDialog(null,"Confirmar --¿Eliminar Registro?");
            if(n==0){   
            MantenimientoCliente mc=new MantenimientoCliente();
            mc.Eliminar(cte);
            }
        }catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public JTable PuenteMostrarTabla (JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        con=new MantenimientoCliente();
        JTable tbl;
        tbl=con.MostrarTabla(tabla);
        
        return tbl; //retornar la tabla llena a la interfaz automovil
    }
    public JTable PuenteBuscarCliente(JTable tb,String cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoCliente mc=new MantenimientoCliente();
            mc.BuscarTb(tb,cte);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        return tb;
    }
}
