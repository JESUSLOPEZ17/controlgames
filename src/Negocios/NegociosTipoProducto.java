package Negocios;

import Datos.TipoProducto;
import Datos.MantenimientoTipoProducto;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class NegociosTipoProducto {
     MantenimientoTipoProducto con;
    
    public void PuenteRegistrar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        try {
            con=new MantenimientoTipoProducto();
            con.Registrar(tp);
            
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void PuenteBuscar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoTipoProducto mc=new MantenimientoTipoProducto();
            mc.Buscar(tp);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void Actualizar(TipoProducto tp){
        try {
            MantenimientoTipoProducto ac=new MantenimientoTipoProducto();
            ac.Actualizar(tp);           
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
        }
    }
    public void PuenteEliminar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
          try {
            int n=JOptionPane.showConfirmDialog(null,"Confirmar --¿Eliminar Registro?");
            if(n==0){   
            MantenimientoTipoProducto mc=new MantenimientoTipoProducto();
            mc.Eliminar(tp);
            }
        }catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public JTable PuenteMostrarTabla (JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        con=new MantenimientoTipoProducto();
        JTable tbl;
        tbl=con.MostrarTabla(tabla);
        
        return tbl; //retornar la tabla llena a la interfaz automovil
    }
    public JTable PuenteBuscarTipoProducto(JTable tb,String cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoTipoProducto mc=new MantenimientoTipoProducto();
            mc.BuscarTb(tb,cte);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        return tb;
    }
}
