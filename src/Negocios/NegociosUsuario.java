package Negocios;

import Datos.MantenimientoUsuario;
import Datos.Usuario;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JTable;

public class NegociosUsuario {
    MantenimientoUsuario con;
    
    public void PuenteRegistrar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        
        try {
            con=new MantenimientoUsuario();
            con.Registrar(usu);
            
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void PuenteBuscar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoUsuario mc=new MantenimientoUsuario();
            mc.Buscar(usu);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public void Actualizar(Usuario usu){
        try {
            MantenimientoUsuario ac=new MantenimientoUsuario();
            ac.Actualizar(usu);           
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
        }
    }
    public void PuenteEliminar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
          try {
            int n=JOptionPane.showConfirmDialog(null,"Confirmar --¿Eliminar Registro?");
            if(n==0){   
            MantenimientoUsuario mc=new MantenimientoUsuario();
            mc.Eliminar(usu);
            }
        }catch (Exception e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    public JTable PuenteMostrarTabla (JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        con=new MantenimientoUsuario();
        JTable tbl;
        tbl=con.MostrarTabla(tabla);
        
        return tbl; //retornar la tabla llena a la interfaz automovil
    }
    public JTable PuenteBuscarUsuario(JTable tb,String usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        try {
            MantenimientoUsuario mc=new MantenimientoUsuario();
            mc.BuscarTb(tb,usu);
              
        }catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        return tb;
    }
}
