package Datos;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MantenimientoUsuario {
      private Statement sentenciaSQL;
    ResultSet rs;
    BDConexion bd;
    
    
    public void Registrar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try{
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sentenciaSQL.executeUpdate("INSERT INTO tblusuario(Nombre,Direccion,Telefono,TipoUsuario,Contraseña,NombreUsuario) VALUES('"+usu.getNombre()+"','"+usu.getDireccion()+"','"+usu.getTelefono()+"','"+usu.getTipousuario()+"','"+usu.getContraseña()+"','"+usu.getNombreu()+"')");
            JOptionPane.showMessageDialog(null,"Registro completo");
            sentenciaSQL.close();
            bd.CerrarConexion();
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        
    }
    public Usuario Buscar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            String sql="SELECT * FROM tblusuario WHERE idUsuario="+usu.getIdUsuario();//apostrofe cuando es char o varchar
            rs=sentenciaSQL.executeQuery(sql);
            
            if(rs.next()){
                usu.setNombre(rs.getString("Nombre"));
                usu.setDireccion(rs.getString("Direccion"));
                usu.setTelefono(rs.getString("Telefono"));
                usu.setTipousuario(rs.getString("TipoUsuario"));
                rs.close();
                bd.CerrarConexion();
            }else{
                 JOptionPane.showMessageDialog(null, "El usuario no existe ");
            }
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
       return usu; 
    }
    public void Actualizar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            String sql="UPDATE tblusuario set idUsuario=?,Nombre=?,Direccion=?,Telefono=?,TipoUsuario=?,Contraseña=?,NombreUsuario=? WHERE idUsuario=?";
            
            PreparedStatement ps=bd.Conectarse().prepareStatement(sql);
            
            ps.setInt(1,usu.getIdUsuario());
            ps.setString(2,usu.getNombre());
            ps.setString(3,usu.getDireccion());
            ps.setString(4,usu.getTelefono());
            ps.setString(5,usu.getTipousuario());
            ps.setString(6,usu.getContraseña());
            ps.setString(7,usu.getNombreu());
            ps.setInt(8,usu.getIdUsuario());
            
            int n=ps.executeUpdate();
            if (n>0){
                JOptionPane.showMessageDialog(null,"Usuario Modificado");
                bd.CerrarConexion();
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
    }
    
    public void Eliminar(Usuario usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        String sql="DELETE FROM tblusuario WHERE idUsuario ="+usu.getIdUsuario();
        try {
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            int n=sentenciaSQL.executeUpdate(sql);
            if(n>0){
                JOptionPane.showMessageDialog(null,"Registro eliminado ");
                bd.CerrarConexion();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
        
    }
     public JTable MostrarTabla(JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        String sql;
        try{
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tblUsuario ";
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();//obtiene el numero de columnas de la tabla
            DefaultTableModel modelo=new DefaultTableModel();//crear un objeto tabla modelo
            for (int i = 1; i <= col; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));//llena el encabezado de la tabla con los nombres de los campos
            }      
//              while(rs.next()){
//             String fila[]=new String[col];
//             for (int j = 0; j <col; j++) {
//                 if(rsm.getColumnLabel(j)=="Contraseña"){
//                    java.security.MessageDigest md = java.security.MessageDigest.getInstance(rs.getString(j));
//                    fila[j]=rs.getString(j+1);//descifrar --- encirptacion hash java
//                 }else{
//                    fila[j]=rs.getString(j+1);
//                 }
//             }
         while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                    fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
     public JTable BuscarTb(JTable tabla,String usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        String sql;
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tblusuario WHERE Nombre like '%"+usu+"%'"; //apostrofe cuando es char o varchar
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();
            DefaultTableModel modelo=new DefaultTableModel();
            for (int i = 0; i <= 6; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));
            }
            while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                 fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
}
