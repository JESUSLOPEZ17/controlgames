package Datos;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class MantenimientoCliente {
    private Statement sentenciaSQL;
    ResultSet rs;
    BDConexion bd;
    
    
    public void Registrar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try{
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sentenciaSQL.executeUpdate("INSERT INTO tblcliente(Nombre,Telefono) VALUES('"+cte.getNombre()+"','"+cte.getTelefono()+"')");
            JOptionPane.showMessageDialog(null,"Registro completo");
            sentenciaSQL.close();
            bd.CerrarConexion();
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        
    }
    public Cliente Buscar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            String sql="SELECT * FROM tblcliente WHERE idCliente="+cte.getIdCliente();//apostrofe cuando es char o varchar
            rs=sentenciaSQL.executeQuery(sql);
            
            if(rs.next()){
                cte.setNombre(rs.getString("Nombre"));
                cte.setTelefono(rs.getString("Telefono"));
                rs.close();
                bd.CerrarConexion();
            }else{
                 JOptionPane.showMessageDialog(null, "El cliente no existe ");
            }
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
       return cte; 
    }
    public void Actualizar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            String sql="UPDATE tblcliente set idCliente=?,Nombre=?,Telefono=? WHERE idCliente=?";
            
            PreparedStatement ps=bd.Conectarse().prepareStatement(sql);
            
            ps.setInt(1,cte.getIdCliente());
            ps.setString(2,cte.getNombre());
            ps.setString(3,cte.getTelefono());
            ps.setInt(4,cte.getIdCliente());
            
            int n=ps.executeUpdate();
            if (n>0){
                JOptionPane.showMessageDialog(null,"Cliente Modificado");
                bd.CerrarConexion();
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
    }
    
    public void Eliminar(Cliente cte) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        String sql="DELETE FROM tblcliente WHERE idCliente ="+cte.getIdCliente();
        try {
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            int n=sentenciaSQL.executeUpdate(sql);
            if(n>0){
                JOptionPane.showMessageDialog(null,"Registro eliminado ");
                bd.CerrarConexion();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
        
    }
     public JTable MostrarTabla(JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        String sql;
        try{
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tblCliente ";
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();//obtiene el numero de columnas de la tabla
            DefaultTableModel modelo=new DefaultTableModel();//crear un objeto tabla modelo
            for (int i = 1; i <= col; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));//llena el encabezado de la tabla con los nombres de los campos
            }      
//              while(rs.next()){
//             String fila[]=new String[col];
//             for (int j = 0; j <col; j++) {
//                 if(rsm.getColumnLabel(j)=="Contraseña"){
//                    java.security.MessageDigest md = java.security.MessageDigest.getInstance(rs.getString(j));
//                    fila[j]=rs.getString(j+1);//descifrar --- encirptacion hash java
//                 }else{
//                    fila[j]=rs.getString(j+1);
//                 }
//             }
         while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                    fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
     public JTable BuscarTb(JTable tabla,String usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        String sql;
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tblcliente WHERE Nombre like '%"+usu+"%'"; //apostrofe cuando es char o varchar
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();
            DefaultTableModel modelo=new DefaultTableModel();
            for (int i = 0; i <= 7; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));
            }
            while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                 fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
}
