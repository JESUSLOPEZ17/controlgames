package Datos;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MantenimientoTipoProducto {
    private Statement sentenciaSQL;
    ResultSet rs;
    BDConexion bd;
    
    
    public void Registrar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try{
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sentenciaSQL.executeUpdate("INSERT INTO tbltipoproducto(TipoProducto,Descuento) VALUES('"+tp.getTipoProducto()+"','"+tp.getDescuento()+"')");
            JOptionPane.showMessageDialog(null,"Registro completo");
            sentenciaSQL.close();
            bd.CerrarConexion();
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        
    }
    public TipoProducto Buscar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            String sql="SELECT * FROM tbltipoproducto WHERE idTipo="+tp.getIdTipo();//apostrofe cuando es char o varchar
            rs=sentenciaSQL.executeQuery(sql);
            
            if(rs.next()){
                tp.setTipoProducto(rs.getString("TipoProducto"));
                tp.setDescuento(rs.getInt("Descuento"));
                rs.close();
                bd.CerrarConexion();
            }else{
                 JOptionPane.showMessageDialog(null, "El producto no existe ");
            }
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
       return tp; 
    }
    public void Actualizar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            String sql="UPDATE tbltipoproducto set idTipo=?,TipoProducto=?,Descuento=? WHERE idTipo=?";
            
            PreparedStatement ps=bd.Conectarse().prepareStatement(sql);
            
            ps.setInt(1,tp.getIdTipo());
            ps.setString(2,tp.getTipoProducto());
            ps.setInt(3,tp.getDescuento());
            ps.setInt(4,tp.getIdTipo());
            
            int n=ps.executeUpdate();
            if (n>0){
                JOptionPane.showMessageDialog(null,"Producto Modificado");
                bd.CerrarConexion();
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
    }
    
    public void Eliminar(TipoProducto tp) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        String sql="DELETE FROM tbltipoproducto WHERE idTipo ="+tp.getIdTipo();
        try {
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            int n=sentenciaSQL.executeUpdate(sql);
            if(n>0){
                JOptionPane.showMessageDialog(null,"Registro eliminado ");
                bd.CerrarConexion();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
        
    }
     public JTable MostrarTabla(JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        String sql;
        try{
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tbltipoproducto ";
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();//obtiene el numero de columnas de la tabla
            DefaultTableModel modelo=new DefaultTableModel();//crear un objeto tabla modelo
            for (int i = 1; i <= col; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));//llena el encabezado de la tabla con los nombres de los campos
            }      
//              while(rs.next()){
//             String fila[]=new String[col];
//             for (int j = 0; j <col; j++) {
//                 if(rsm.getColumnLabel(j)=="Contraseña"){
//                    java.security.MessageDigest md = java.security.MessageDigest.getInstance(rs.getString(j));
//                    fila[j]=rs.getString(j+1);//descifrar --- encirptacion hash java
//                 }else{
//                    fila[j]=rs.getString(j+1);
//                 }
//             }
         while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                    fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
     public JTable BuscarTb(JTable tabla,String usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        String sql;
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tbltipoproducto WHERE TipoProducto like '%"+usu+"%'"; //apostrofe cuando es char o varchar
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();
            DefaultTableModel modelo=new DefaultTableModel();
            for (int i = 0; i <= 7; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));
            }
            while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                 fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
}
