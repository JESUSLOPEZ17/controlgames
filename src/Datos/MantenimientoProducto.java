package Datos;

import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MantenimientoProducto {
       private Statement sentenciaSQL;
    ResultSet rs;
    BDConexion bd;
    
    
    public void Registrar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try{
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sentenciaSQL.executeUpdate("INSERT INTO tblproducto(Codigo,Nombre,Precio,Cantidad,IdTipo) VALUES('"+pro.getCodigo()+"','"+pro.getNombre()+"','"+pro.getPrecio()+"','"+pro.getCantidad()+"','"+pro.getIdTipo()+"')");
            JOptionPane.showMessageDialog(null,"Registro completo");
            sentenciaSQL.close();
            bd.CerrarConexion();
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
        
    }
    public Producto Buscar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            String sql="SELECT * FROM tblproducto WHERE Codigo="+pro.getCodigo();//apostrofe cuando es char o varchar
            rs=sentenciaSQL.executeQuery(sql);
            
            if(rs.next()){
                pro.setNombre(rs.getString("Nombre"));
                pro.setPrecio(rs.getDouble("Precio"));
                pro.setCantidad(rs.getInt("Cantidad"));
                pro.setIdTipo(rs.getInt("IdTipo"));
                rs.close();
                bd.CerrarConexion();
            }else{
                 JOptionPane.showMessageDialog(null, "El producto no existe ");
            }
        } catch (SQLException e) {
             JOptionPane.showMessageDialog(null, "Error: "+e);
        }
       return pro; 
    }
    public void Actualizar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        try {
            String sql="UPDATE tblproducto set Codigo=?,Nombre=?,Precio=?,Cantidad=?,idTipo=? WHERE Codigo=?";
            
            PreparedStatement ps=bd.Conectarse().prepareStatement(sql);
            
            ps.setString(1,pro.getCodigo());
            ps.setString(2,pro.getNombre());
            ps.setDouble(3,pro.getPrecio());
            ps.setInt(4,pro.getCantidad());
            ps.setInt(5,pro.getIdTipo());
            ps.setString(6,pro.getCodigo());
            
            int n=ps.executeUpdate();
            if (n>0){
                JOptionPane.showMessageDialog(null,"Producto Modificado");
                bd.CerrarConexion();
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
    }
    
    public void Eliminar(Producto pro) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        String sql="DELETE FROM tblproducto WHERE Codigo ="+pro.getCodigo();
        try {
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            int n=sentenciaSQL.executeUpdate(sql);
            if(n>0){
                JOptionPane.showMessageDialog(null,"Registro eliminado ");
                bd.CerrarConexion();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error "+e);
        }
        
    }
     public JTable MostrarTabla(JTable tabla) throws ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchAlgorithmException{
        String sql;
        try{
            bd=new BDConexion();
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tblproducto ";
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();//obtiene el numero de columnas de la tabla
            DefaultTableModel modelo=new DefaultTableModel();//crear un objeto tabla modelo
            for (int i = 1; i <= col; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));//llena el encabezado de la tabla con los nombres de los campos
            }      
//              while(rs.next()){
//             String fila[]=new String[col];
//             for (int j = 0; j <col; j++) {
//                 if(rsm.getColumnLabel(j)=="Contraseña"){
//                    java.security.MessageDigest md = java.security.MessageDigest.getInstance(rs.getString(j));
//                    fila[j]=rs.getString(j+1);//descifrar --- encirptacion hash java
//                 }else{
//                    fila[j]=rs.getString(j+1);
//                 }
//             }
         while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                    fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
     public JTable BuscarTb(JTable tabla,String usu) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException{
        bd=new BDConexion();
        String sql;
        try {
            sentenciaSQL=bd.Conectarse().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            sql="SELECT * FROM tblproducto WHERE Nombre like '%"+usu+"%'"; //apostrofe cuando es char o varchar
            ResultSet rs=sentenciaSQL.executeQuery(sql);
            ResultSetMetaData rsm=rs.getMetaData();
            int col=rsm.getColumnCount();
            DefaultTableModel modelo=new DefaultTableModel();
            for (int i = 0; i <= 7; i++) {
                modelo.addColumn(rsm.getColumnLabel(i));
            }
            while(rs.next()){
             String fila[]=new String[col];
             for (int j = 0; j <col; j++) {
                 fila[j]=rs.getString(j+1);
             }
             modelo.addRow(fila);
         }   
          tabla.setModel(modelo);
          rs.close();
          bd.CerrarConexion();
        } catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }   
        return tabla;
    }
}
