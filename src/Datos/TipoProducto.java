package Datos;


public class TipoProducto {
    private int idTipo;
    private String TipoProducto;
    private int Descuento;

    public TipoProducto(int idTipo, String TipoProducto, int Descuento) {
        this.idTipo = idTipo;
        this.TipoProducto = TipoProducto;
        this.Descuento = Descuento;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getTipoProducto() {
        return TipoProducto;
    }

    public void setTipoProducto(String TipoProducto) {
        this.TipoProducto = TipoProducto;
    }

    public int getDescuento() {
        return Descuento;
    }

    public void setDescuento(int Descuento) {
        this.Descuento = Descuento;
    }
    
}
