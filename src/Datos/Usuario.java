package Datos;

public class Usuario {
    private int idUsuario;
    private String nombre;
    private String direccion;
    private String telefono;
    private String tipousuario;
    private String contraseña;
    private String nombreu;
    
     public Usuario(int idUsuario,String nombre,String direccion,String telefono,String tipousuario,String contraseña,String nombreu) {
        this.idUsuario=idUsuario;
        this.nombre=nombre;
        this.direccion=direccion;
        this.telefono=telefono;
        this.tipousuario=tipousuario;
        this.contraseña=contraseña;
        this.nombreu=nombreu;    
    }
    public Usuario(){
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTipousuario() {
        return tipousuario;
    }

    public void setTipousuario(String tipousuario) {
        this.tipousuario = tipousuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombreu() {
        return nombreu;
    }

    public void setNombreu(String nombreu) {
        this.nombreu = nombreu;
    }
    
    
}
